import 'package:flutter/material.dart';

Row buildPersonData(IconData icon, String head, String data) {
  return Row(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: Colors.black,
      ),
      Container(
        padding: const EdgeInsets.only(left: 10),
        child: Text(
          head,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
      ),
      Expanded(
          child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            padding: EdgeInsets.only(left: 0),
            child: Text(
              data,
              maxLines: 1,
              style: TextStyle(
                fontSize: 14,
              ),
            ),
          ),
        ],
      )),
    ],
  );
}

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
        child: Column(
          children: [
            Container(
              child: buildPersonData(
                  Icons.account_circle, 'ชื่อ', 'ธวัชชัย ท่าจีน'),
            ),
            Container(
              child: buildPersonData(
                  Icons.cake, 'วันเกิด', '6 กรกฎาคม พ.ศ.2543'),
            ),
            Container(
              child: buildPersonData(Icons.male, 'เพศ', 'ชาย'),
            ),
            Container(
              child: buildPersonData(Icons.home, 'ที่อยู่',
                  '456/93 ต.แพรกษใหม่ อ.เมือง จ.สมุทรปราการ 10280'),
            ),
            Container(
              child: buildPersonData(
                  Icons.email, 'Email', 'Blackwingdy@gmail.com'),
            ),
            Container(
              child: buildPersonData(Icons.phone, 'เบอร์โทร', '064-1104853'),
            ),
          ],
        )
);


buildTitle(IconData icon,String label){
  return Row(
    mainAxisSize: MainAxisSize.min, children: [
      Icon(
        icon,
        color: Colors.black,
      ),
      Expanded(
        child: Container(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      )
    ],
  );
}

void main (){
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  @override
  
  Widget build(BuildContext context) {
    
    Widget educationinfo = Container(
      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
      child: buildPersonData(Icons.book, 'การศึกษาระดับปริญญาตรี', 'มหาวิทยาลัยบูรพา'),
      
    );
    
    Widget titleSkill = Container(
      padding: const EdgeInsets.fromLTRB(32, 0, 0, 0),
      child: buildTitle(Icons.computer, 'ทักษะ'),
    );

    Widget titleEducation = Container(
      padding: const EdgeInsets.fromLTRB(32, 0, 0, 0),
      child: buildTitle(Icons.school,'การศึกษา'),
    );

     Widget listSkill = Container(
       padding: const EdgeInsets.fromLTRB(50, 15, 0, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'images/C++.png',
            height: 80,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/C#.png',
            height: 80,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/java.png',
            height: 80,
            fit: BoxFit.contain,
          ),
        ],
      ),
    );

    return MaterialApp(
      title: 'Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Resume'),
        ),
        body: ListView(children: [
          Image.asset(
            'images/Thawatchai.jpg',
            height: 240,
            fit: BoxFit.fitHeight,
          ),
          titleSection,
          titleSkill,
          listSkill,
          titleEducation,
          educationinfo
        ]),
      )
    );
  }
  
}